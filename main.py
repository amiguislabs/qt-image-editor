import sys

from PyQt4 import QtGui

from gui import Window
from constants import WINDOW


def main():
    app = QtGui.QApplication(sys.argv)
    gui = Window(**WINDOW)
    gui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
